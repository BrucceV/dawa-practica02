const express = require('express')
const app = express()
const morgan = require('morgan')
const path = require('path')
const async = require('express-async-await')
const fetch = require('node-fetch')
const bodyParser = require('body-parser');
const mongoose = require('./database')

//My Modules
const formValidation = require('./public/modules/formValidation')


/* Settings */
app.set('port', process.env.PORT || 3000)


/* Middlewares */
app.use(morgan('dev'))
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: false }));


/* Static files */
app.use(express.static(__dirname + '/public'))
app.set('view engine', 'pug')


/* Routes */
app.use('/api/posts', require('./routes/Posts/post.routes'))
app.use('/api/users', require('./routes/Users/user.routes'))


const getData = async() => {
  try{
    const allData = await fetch('https://yts.mx/api/v2/list_movies.json')
    const dataToJson = await allData.json()
    const movies = await dataToJson.data.movies
    return movies
  }catch(error){
    console.log(error)
  }
}

function mayorMenor (miLista){
  const ordenada = miLista
  for(let i = miLista.length - 1; i > 0; i--){
    for(let e = 0; e < i; e++){
      if(ordenada[e].rating > ordenada[e + 1].rating){
        let actual = ordenada[e]
        ordenada.splice(e+2,0, actual)
        ordenada.splice(e, 1)
      }else{
        continue
      }
    }
  }
  return ordenada
}


app.get('/', async (req, res) => {
  try{
    const movies = await getData()
    const mostPopular = mayorMenor(movies)
    const recomended = [mostPopular[mostPopular.length - 1], mostPopular[mostPopular.length - 2]]
    res.render(path.join(__dirname + '/Views/index.pug'), {movies: recomended})
  }catch(error){
    console.log(error)
  }

})

app.get('/aboutUs', async (req, res) => {
  res.render(path.join(__dirname + '/Views/aboutUs.pug'))
})

app.get('/notice', async (req, res) => {
  try{
    const movies = await getData()
    const genres = ["Drama", "Romance", "Comedy", "Action"]
    res.render(path.join(__dirname + '/Views/notice.pug'), {movies: movies, genres: genres})
  }catch(error){
    console.log(error)
  }

})

app.get('/blog', async (req, res) => {
  try{
    const movies = await getData()
    const genres = ["Drama", "Romance", "Comedy", "Action"]
    res.render(path.join(__dirname + '/Views/blog.pug'), {movies: movies, genres: genres})
  }catch(error){
    console.log(error)
  }
})

app.get('/blogDetails', async (req, res) => {
  try{
    const movies = await getData()
    var id = req.query.id;
    res.render(path.join(__dirname + '/Views/blogDetails.pug'), {movies: movies, id: id})
  }catch(error){
    console.log(error)
  }
})

app.get('/contactUs', async (req, res) => {
  res.render(path.join(__dirname + '/Views/contactUs.pug'))
})



app.post('/answer', (req, res) => {

  const {validationIsSuccess, dataForm} = formValidation.validateFields(req.body)

  if(validationIsSuccess){
    res.render(path.join(__dirname + '/Views/contactUsAnswer.pug'), dataForm)
  }else{
    res.render(path.join(__dirname + '/Views/contactUs.pug'), dataForm)
  }
})


app.listen(app.get('port'), () => console.log(`La aplicacion esta escuchando en el puerto ${app.get('port')}`))