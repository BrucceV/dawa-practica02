const mongoose = require('mongoose')

mongoose.connect(
  'mongodb://localhost:27017/Practica02',
  {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true})
  .then( _ => {
    console.log('Base de datos conectada')
  })
  .catch(error => {
    console.log(error)
  })

module.exports = mongoose