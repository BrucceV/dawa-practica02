const mongoose = require('mongoose')
const schema = mongoose.Schema

const validCategories = {
  values: [
    'Deportes',
    'General'
  ],
  message: '(VALUE) no es un rol valido'
}

const postSchema = new schema({
  url: {
    type: String,
  },
  category: {
    type: String,
    default: 'General',
    enum: validCategories
  },
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  creation_date: {
    type: Date,
    default: new Date()
  },
  comments: [{
    type: schema.Types.ObjectId,
    ref: 'Comment'
  }],
  userBelong: {
    type: schema.Types.ObjectId,
    ref: 'User'
  }
})


module.exports = mongoose.model('Post', postSchema)