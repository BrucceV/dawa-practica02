const mongoose = require('mongoose')
const schema = mongoose.Schema

const commentSchema = new schema({
  content: {
    type: String,
    required: true
  },
  postBelong: {
    type: schema.Types.ObjectId,
    ref: 'Post'
  },
  userSender: {
    type: schema.Types.ObjectId,
    ref: 'User'
  }
})


module.exports = mongoose.model('Comment', commentSchema)