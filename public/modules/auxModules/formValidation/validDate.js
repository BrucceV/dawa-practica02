exports.calcularEdad = (fechaNacimiento) => {
  const currentDate = new Date()
  
  const differenceInTime = currentDate.getTime() - fechaNacimiento.getTime()
  const differenceInYears = differenceInTime / (1000 * 3600 * 24 * 365)

  if(differenceInYears > 18){
    return true
  }else {
    return false
  }
}