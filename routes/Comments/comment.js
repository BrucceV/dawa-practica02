const Post = require('../../models/post')
const User = require('../../models/user')
const Comment = require('../../models/comment')

module.exports = {
  newComment : async (req, res) => {
    try{
      const newComment = new Comment(req.body)
  
      const commentSaved = await newComment.save()
  
      res.json({
        ok: true,
        status: 'Comment Saved Successfully',
        commentSaved
      })
      
    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
      })
    }
  },

  newPostComment: async (req, res, next) => {
    try{
      const {postId, userId} = req.params;
      const newComment = new Comment(req.body)
      
      /* Getting the post which created the post */
      const post = await Post.findById(postId)
      console.log(post)
      /* Storing the post in the newComment collection */
      newComment.postBelong = post

      /* Getting the user whose comment in the post */
      const user = await User.findById(userId)
      /* Storing the user into the newComment collection */
      newComment.userSender = user

      /* Saving the new comment on the database */
      const commentSaved = await newComment.save()
      console.log(commentSaved)
      /* Storing the new post into the users Collection */
      post.comments.push(newComment)
      /* Saving the new update to users */
      await post.save()

      res.json({
        ok: true,
        status: 'Comment Saved Successfully',
        commentSaved
      })
      
    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
      })
    }
  },

/*   deleteCommentFromPost: async (req, res) => {
    try{
      const {postId}
    }catch(error){

    }
  },
 */
  getComments : async (req, res) => {
    try{
      const comments = await Comment.find({})
      res.json({
        ok: true,
        comments
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error,
      })
    }
  },

  deleteComment : async (req, res) => {
    try{
      const {commentId} = req.params
      const commentDeleted = await Comment.findByIdAndDelete(commentId)
      res.json({
        ok: true,
        commentDeleted
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error
      })
    }
  },

  updateComment : async (req, res) => {
    try{
      const {commentId} = req.params
      const commentUpdated = await Comment.findByIdAndUpdate(postId, req.body, {new: true})
      res.status(200).json({
        ok: true,
        message: 'Post updated.',
        commentUpdated
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error
      })
    }
  }
}