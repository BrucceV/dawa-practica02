const Post = require('../../models/post')
const User = require('../../models/user')

module.exports = {
  newPost : async (req, res) => {
    try{
      const {title, content, category, url} = req.body
      const post = new Post({
        title,
        content,
        category,
        url
      })
  
      const postSaved = await post.save()
  
      res.json({
        ok: true,
        status: 'User Saved Successfully',
        postSaved
      })
      
    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
      })
    }
  },

  newUserPost: async (req, res) => {
    try{
      const {userId} = req.params;
      const newPost = new Post(req.body)
      
      /* Getting the user whose created the post */
      const user = await User.findById(userId)
      console.log(user)
      /* Storing the user in the newPost collection */
      newPost.userBelong = user
      /* Saving the new post on the database */
      const postSaved = await newPost.save()
      console.log(postSaved)
      /* Storing the new post into the users Collection */
      user.posts.push(newPost)
      /* Saving the new update to users */
      await user.save()

      res.json({
        ok: true,
        status: 'User Saved Successfully',
        postSaved
      })
      
    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
      })
    }
  },

  getPosts : async (req, res) => {
    try{
      const posts = await Post.find({}).populate('comments')
      res.json({
        ok: true,
        posts
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error,
      })
    }
  },

  deletePost : async (req, res) => {
    try{
      const {postId} = req.params
      const postDeleted = await Post.findByIdAndDelete(postId)
      res.json({
        ok: true,
        postDeleted
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error
      })
    }
  },

  updatePost : async (req, res) => {
    try{
      const {postId} = req.params
      const body = req.body
      const postUpdated = await Post.findByIdAndUpdate(postId, body, {new: true})
      res.status(200).json({
        ok: true,
        message: 'Post updated.',
        postUpdated
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error
      })
    }
  }
}